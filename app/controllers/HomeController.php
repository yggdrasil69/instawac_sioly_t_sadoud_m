<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function logout()
	{
		Auth::logout();
		return View::make('connexion');
	}

	public function connexion()
	{
		if(!Auth::check()){
			return View::make('connexion');
		}else{
			return View::make('accueil');
		}
	}

	public function goconnexion(){
		$login = Input::get('login');
		$password = Input::get('password');

		if(Auth::attempt(array('login' => $login, 'password' => $password)))
			return Redirect::to('accueil');
		else
			echo "connexion FAIL";
	}

	public function inscription()
	{
		if(!Auth::check()){
			return View::make('inscription');
		}else{
			return View::make('accueil');
		}
	}

	public function goinscription(){
		$login = Input::get('login');
		$password = Hash::make(Input::get('password'));
		$name = Input::get('name');
		$lastname = Input::get('lastname');
		$mail = Input::get('mail');
		$birthdate = Input::get('birthdate');
		DB::table('users')->insert(array('login'=>$login,'password'=>$password,'name'=>$name,'lastname'=>$lastname,'email'=>$mail,'birthdate'=>$birthdate,'date'=>date("Y-m-d-H-i-s")));
		return View::make('connexion');
	}

	public function accueil(){
		if(Auth::check()){
			return View::make('accueil');
		}else{
			return View::make('connexion');
		}
	}

	public function catalogue(){
		if(Auth::check()){
			return View::make('catalogue');
		}else{
			return View::make('connexion');
		}
	}

	function search(){
		return View::make('search');
	}

	function update($id){
		return View::make('update', compact('id'));
	}

	function edit($id){
		$title = Input::get('title');
		$content = Input::get('content');
		$price = Input::get('price');
		$file = Input::file('file');

		if(!empty($file)){
			$filename = $file->getClientOriginalName();
			$destinationPath = 'images';
			$type = $file->getClientMimeType();
			DB::table('item')->WHERE('id',$id)->update(array('id_user'=>3,'title'=>$title,'content'=>$content,'price'=>$price,'file'=>$filename));
			$file->move($destinationPath, $filename);
			return Redirect::to('catalogue');
		}else{
			DB::table('item')->WHERE('id',$id)->update(array('id_user'=>3,'title'=>$title,'price'=>$price,'content'=>$content));
			return Redirect::to('catalogue');
		}
	}

	function delete($id){
		DB::table('item')->where('id',$id)->delete();
		return View::make('catalogue');
	}

}
