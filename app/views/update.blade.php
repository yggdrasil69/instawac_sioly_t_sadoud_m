<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>E-commerce</title>
		<meta name="description" content="E-commerce"/>
		<meta name="keywords" content="E-commerce, commerce, vente"/>
		{{HTML::style('css/css.css')}}
		{{HTML::script('js/style.js')}}
	</head>
	<body>
		<div id="menu">
			<span><?php if(Auth::check()){ echo "Bienvenue ".Auth::user()->login;}?></span> {{HTML::LINK('/catalogue','Catalogue')}} <?php if(Auth::check()){ ?> {{HTML::LINK('/logout','Se déconnecter',array('class'=>'logout'))}}<?php } ?>
			<strong>{{Form::open(array('url' => 'search'))}}
				<select name="category">
						<?php $cat = DB::table('category')->get();
							foreach ($cat as $c){
								echo '<option value="'.$c->nom.'">'.$c->nom.'</option>';
							}
						?>
				</select>
				<select name="souscategory">
						<?php $cat = DB::table('souscategory')->get();
							foreach ($cat as $c){
								echo '<option value="'.$c->nom.'">'.$c->nom.'</option>';
							}
						?>
				</select>
				<input type="text" name="search" placeholder="Valider avec la touche entrée"/>
			{{form::close()}}</strong>
		</div>
		<?php $update = DB::table('item')->where('id',$id)->get();
			foreach ($update as $up){
				if($up->file != ""){
					echo "<div class='pic'>".HTML::image('../public/images/'.$up->file, 'picture')."</div>";
					echo "<div class='update'><strong>".$up->title."</strong><br/><span>".$up->content."</span><br/><small>".$up->price." euros<br/>Vendu par : ".$up->Seller."</small></div>";
					?><div id='edit'><?php
					if(Auth::user()->droit == 3){
						?><a onClick="show('update')">Modifier</a><?php
						echo "<br/>".HTML::link('/delete/'.$id, 'Supprimer');
					}
					?></div><?php
				}else{
					echo "<div class='pic'>".HTML::image('../public/images/notimg.png', 'picture')."</div>";
					echo "<div class='update'><strong>".$up->title."</strong><br/><span>".$up->content."</span><br/><small>".$up->price." euros<br/>Vendu par : ".$up->Seller."</small></div>";
					?><div id='edit'><?php
					if(Auth::user()->droit == 3){
						?><a onClick="show('update')">Modifier</a><?php
						echo "<br/>".HTML::link('/delete/'.$id, 'Supprimer');
					}
					?></div><?php
				}
			}
		?>
		<div id="update">
			{{Form::open(array('url' => '/edit/'.$id,'files'=>true))}}
			<label for="title">Titre</label><input type="text" required pattern="[a-zA-Z èéçàù]{2,50}" name="title" /><br/>
			<label for="content">Description</label><textarea name="content" placeholder="Entre 10 et 1000 caractères" required pattern="[a-zA-Z0-9 èéçàù.;:?!-,@]{10,1000}" ></textarea><br/>
			<label for="price">Prix</label><input type="text" required pattern="[0-9]+" name="price" /><br/>
			<label for="file">Image</label><input type="file" name="file" /></br>
			<input type="submit" name="submit" value="Modifier"/>
			{{form::close()}}
		</div>
	</body>
</html>
