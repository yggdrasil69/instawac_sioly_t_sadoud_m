<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>E-commerce</title>
		{{HTML::style('css/css.css')}}
	</head>
	<body>
		<div id="menu">
			<span><?php if(Auth::check()){ echo "Bienvenue ".Auth::user()->login;}?></span> {{HTML::LINK('/','Connexion')}} {{HTML::LINK('/inscription','inscription')}} <?php if(Auth::check()){ ?> {{HTML::LINK('/logout','Se déconnecter',array('class'=>'logout'))}}<?php } ?>
		</div>
		<div id="inscription">
			{{Form::open(array('url' => 'goinscription'))}}
				<label for="login">Login</label><input type="text" name="login" placeholder="Entre 2 et 20 caractères" required pattern="[a-zA-Z0-9]{2,20}"/><br/>
				<label for="password">Password</label><input type="text" name="password" required pattern="[a-zA-Z0-9èéçàù]{2,20}"/><br/>
				<label for="name">Name</label><input type="text" name="name" required pattern="[a-zA-Z0-9èéçàù]{2,50}"/><br/>
				<label for="lastname">Lastame</label><input type="text" name="lastname" required pattern="[a-zA-Z0-9èéçàù]{2,50}"/><br/>
				<label for="mail">Mail</label><input type="text" name="mail" required pattern="[a-zA-Z0-9._-]+@[a-z0-9._-]{2,9}\.[a-z]{2,4}"/><br/>
				<label for="birthdate">Anniversaire</label><input type="text" name="birthdate" required ="true"/><br/>
				<input type="submit" name="submit"/>
			{{form::close()}}
		</div>
	</body>
</html>
