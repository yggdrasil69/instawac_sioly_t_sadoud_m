<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>E-commerce</title>
		{{HTML::style('css/css.css')}}
	</head>
	<body>
		<div id="menu">
			<span><?php if(Auth::check()){ echo "Bienvenue ".Auth::user()->login;}?></span> {{HTML::LINK('/','Connexion')}} {{HTML::LINK('/inscription','inscription')}} <?php if(Auth::check()){ ?> {{HTML::LINK('/logout','Se déconnecter',array('class'=>'logout'))}}<?php } ?>
		</div>
		<div id="connexion">
			{{Form::open(array('url' => 'goconnexion'))}}
				<label for="login">Login</label><input type="text" name="login" required pattern="[a-zA-Z0-9]{2,20}"/><br/>
				<label for="password">Password</label><input type="text" name="password" required pattern="[a-zA-Z0-9èéçàù]{2,20}"/><br/>
				<input type="submit" name="submit">
			{{form::close()}}
		</div>
	</body>
</html>
