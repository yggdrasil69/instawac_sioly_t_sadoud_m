<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>E-commerce</title>
		<meta name="description" content="E-commerce"/>
		<meta name="keywords" content="E-commerce, commerce, vente"/>
		{{HTML::style('css/css.css')}}
		{{HTML::script('js/style.js')}}
	</head>
	<body>
		<div id="menu">
			<span><?php if(Auth::check()){ echo "Bienvenue ".Auth::user()->login;}?></span> {{HTML::LINK('/catalogue','Catalogue')}} <?php if(Auth::check()){ ?> {{HTML::LINK('/logout','Se déconnecter',array('class'=>'logout'))}}<?php } ?>
			<strong>{{Form::open(array('url' => 'search'))}}
				<select name="category">
						<?php $cat = DB::table('category')->get();
							foreach ($cat as $c){
								echo '<option value="'.$c->nom.'">'.$c->nom.'</option>';
							}
						?>
				</select>
				<select name="souscategory">
						<?php $cat = DB::table('souscategory')->get();
							foreach ($cat as $c){
								echo '<option value="'.$c->nom.'">'.$c->nom.'</option>';
							}
						?>
				</select>
				<input type="text" name="search" placeholder="Valider avec la touche entrée"/>
			{{form::close()}}</strong>
		</div>
		<div id="menucategorie">
			<h4>Accessoires</h4>
			<?php $category = DB::table('souscategory')->where('name_category', 'accessoires')->get();
				foreach ($category as $ca){
					echo "<a href=''>".$ca->nom.'</a><br/>';
				}
			?>
			<h4>Appareils photos</h4>
			<?php $category = DB::table('souscategory')->where('name_category', 'media')->get();
				foreach ($category as $ca){
					echo "<a href=''>".$ca->nom.'</a><br/>';
				}
			?>
			<h4>Audio et Hi-fi</h4>
			<?php $category = DB::table('souscategory')->where('name_category', 'audio')->get();
				foreach ($category as $ca){
					echo "<a href=''>".$ca->nom.'</a><br/>';
				}
			?>
			<h4>GPS</h4>
			<?php $category = DB::table('souscategory')->where('name_category', 'gps')->get();
				foreach ($category as $ca){
					echo "<a href=''>".$ca->nom.'</a><br/>';
				}
			?>
			<h4>Objects connectés</h4>
			<?php $category = DB::table('souscategory')->where('name_category', 'connect')->get();
				foreach ($category as $ca){
					echo "<a href=''>".$ca->nom.'</a><br/>';
				}
			?>
			<h4>Petit audio</h4>
			<?php $category = DB::table('souscategory')->where('name_category', 'petit')->get();
				foreach ($category as $ca){
					echo "<a href=''>".$ca->nom.'</a><br/>';
				}
			?>
			<h4>Téléphonie, mobile</h4>
			<?php $category = DB::table('souscategory')->where('name_category', 'mobile')->get();
				foreach ($category as $ca){
					echo "<a href=''>".$ca->nom.'</a><br/>';
				}
			?>
			<h4>Vidéo, home cinéma</h4>
			<?php $category = DB::table('souscategory')->where('name_category', 'video')->get();
				foreach ($category as $ca){
					echo "<a href=''>".$ca->nom.'</a><br/>';
				}
			?>
		<div>
	</body>
</html>
