<!DOCTYPE HTML>
<html>
	<head>
		<title>E-commerce</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta name="description" content="E-commerce"/>
		<meta name="keywords" content="E-commerce, commerce, vente"/>
		{{HTML::style('css/css.css')}}
		{{HTML::script('js/style.js')}}
	</head>
	<body>
		<div id="menu">
			<span><?php if(Auth::check()){ echo "Bienvenue ".Auth::user()->login;}?></span> {{HTML::LINK('/catalogue','Catalogue')}} <?php if(Auth::check()){ ?> {{HTML::LINK('/logout','Se déconnecter',array('class'=>'logout'))}}<?php } ?>
			<strong>{{Form::open(array('url' => 'search'))}}
				<select name="category">
						<?php $cat = DB::table('category')->get();
							foreach ($cat as $c){
								echo '<option value="'.$c->nom.'">'.$c->nom.'</option>';
							}
						?>
				</select>
				<select name="souscategory">
						<?php $cat = DB::table('souscategory')->get();
							foreach ($cat as $c){
								echo '<option value="'.$c->nom.'">'.$c->nom.'</option>';
							}
						?>
				</select>
				<input type="text" name="search" placeholder="Valider avec la touche entrée"/>
			{{form::close()}}</strong>
		</div>
		<?php
		$search = $_POST['search'];
		$cat = $_POST['category'];
		$souscat = $_POST['souscategory'];
		$recherche = htmlspecialchars($_POST['search']);
					try 
					{
					    $bdd = new PDO('mysql:host=localhost;dbname=e-commerce;charset=UTF8', 'root', '');
					}
					catch(Exception $e)
					{
					    die('Erreur : '.$e->getMessage());
					}
		
		if($cat != "Catégories" && $souscat == "Sous-catégories"){
			$req = $bdd->prepare("SELECT * FROM item WHERE title LIKE '%$search%' AND category LIKE '$cat'");
			$req->execute(array('search' => $recherche . '%'));
		}elseif($cat != "Catégories" && $souscat != "Sous-catégories"){
			$req = $bdd->prepare("SELECT * FROM item WHERE title LIKE '%$search%' AND category LIKE '$cat' AND souscategory LIKE '$souscat'");
			$req->execute(array('search' => $recherche . '%'));
		}elseif($cat == "Catégories" && $souscat == "Sous-catégories"){
			$req = $bdd->prepare("SELECT * FROM item WHERE title LIKE '%$search%'");
			$req->execute(array('search' => $recherche . '%'));
		}
		while($data = $req->fetch()){
			if($data['file'] != ""){
					echo '<div class="picture">'.HTML::image('../public/images/'.$data['file'], 'picture').html::link('/update/'.$data['id'], $data['title']).'</div>';
			}else{
					echo '<div class="picture">'.HTML::image('../public/images/notimg.png', 'picture').html::link('/update/'.$data['id'], $data['title']).'</div>';
			}
		}
?>
	</body>
</html>