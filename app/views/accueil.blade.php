<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>E-commerce</title>
		{{HTML::style('css/css.css')}}
	</head>
	<body>
		<div id="menu">
			<span><?php if(Auth::check()){ echo "Bienvenue ".Auth::user()->login;}?></span> {{HTML::LINK('/catalogue','Catalogue')}} <?php if(Auth::check()){ ?> {{HTML::LINK('/logout','Se déconnecter',array('class'=>'logout'))}}<?php } ?>
		</div>
	</body>
</html>
