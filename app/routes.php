<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@connexion');
Route::post('/goconnexion', 'HomeController@goconnexion');
Route::get('/inscription', 'HomeController@inscription');
Route::post('/goinscription', 'HomeController@goinscription');
Route::get('/accueil', 'HomeController@accueil');
Route::get('/catalogue', 'HomeController@catalogue');
Route::get('/logout', 'HomeController@logout');
Route::post('/search', 'HomeController@search');
Route::get('/update/{id}', 'HomeController@update');
Route::post('/edit/{id}', 'HomeController@edit');
Route::get('/delete/{id}', 'HomeController@delete');



